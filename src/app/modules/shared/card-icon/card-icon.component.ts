import {Component, Input, OnInit} from "@angular/core";

@Component({
  selector: "card-icon",
  templateUrl: "./card-icon.component.html",
  styleUrls: [],
})
export class CardIcon implements OnInit {
  @Input() public title: string;
  @Input() public number: string;
  @Input() public backgroundColor: string;
  @Input() public color: string;
  @Input() public icon: string;
  @Input() public img_src: string;

  constructor() {
  }

  ngOnInit() {
  }

  ngOnDestroy() {
  }
}
