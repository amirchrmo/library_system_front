import { Component, HostListener, Input, OnInit } from "@angular/core";

@Component({
  selector: "[sortable-column]",
  templateUrl: "./sort-table.component.html",
})
export class SortTableComponent implements OnInit {
  @Input("sortable-column") columnName: string;
  @Input("sort-direction") sortDirection: string = "";

  constructor() {}

  @HostListener("click") sort() {
    this.sortDirection = this.sortDirection === "asc" ? "desc" : "asc";
  }

  ngOnInit() {}
}
