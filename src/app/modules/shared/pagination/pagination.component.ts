import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  SimpleChanges,
} from "@angular/core";

@Component({
  selector: "pagination",
  templateUrl: "./pagination.component.html",
  styleUrls: [],
})
export class PaginationComponent implements OnInit {
  @Input() currentPage: number;
  @Input() totalPages: number;
  @Input() totalCount: number;
  @Output() callback: EventEmitter<number> = new EventEmitter<number>();
  @Output() perPageCallback: EventEmitter<number> = new EventEmitter<number>();
  public visibility: boolean;
  public pages: number[];

  constructor() {}

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges) {
    this._generateList();
  }

  public changePage(pageNumber: number): void {
    if (pageNumber == this.currentPage) return;
    if (pageNumber < 1) this.callback.emit(1);
    else if (pageNumber > this.totalPages) this.callback.emit(this.totalPages);
    else this.callback.emit(pageNumber);
  }

  public changePerPage(val) {
    this.perPageCallback.emit(Number(val));
  }
  public options = [
    { name: "10", id: 1, selected: false },
    { name: "25", id: 2, selected: false },
    { name: "50", id: 3, selected: false },
    { name: "100", id: 4, selected: false },
    { name: "500", id: 5, selected: false },
    { name: "1000", id: 6, selected: false },
  ];
  private _generateList(): void {
    if (
      !this.totalPages ||
      this.totalPages <= 1 ||
      !this.currentPage ||
      this.currentPage <= 0 ||
      this.currentPage > this.totalPages
    ) {
      this.visibility = false;
      return;
    }
    this.visibility = true;

    this.pages = [];
    let index: number;

    if (this.totalPages > 5 && this.currentPage >= 3)
      if (this.currentPage + 2 <= this.totalPages) index = this.currentPage - 2;
      else index = this.totalPages - 4;
    else index = 1;

    while (index <= this.totalPages && this.pages.length < 5) {
      this.pages.push(index);
      index++;
    }
  }
}
