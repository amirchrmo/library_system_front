import { ModuleWithProviders, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
// Third party
import { TranslateModule } from "@ngx-translate/core";
import { OrderModule } from "ngx-order-pipe";
import { DpDatePickerModule } from "ng2-jalali-date-picker";
import { NgxSmartModalModule } from "ngx-smart-modal";
// Components
import { BreadcrumbComponent } from "./breadcrumb/breadcrumb.component";
import { CardIcon } from "./card-icon/card-icon.component";
import { PaginationComponent } from "./pagination/pagination.component";
import { TagInputComponent } from "./tag-input/tag-input.component";
import { SelectInputComponent } from "./select-input/select-input.component";
import { HeaderSelectInputComponent } from "./header-select-input/header-select-input.component";
import { SortTableComponent } from "./sort-table/sort-table.component";
import { InputItemComponent } from "./input-item/input-item.component";
import { ProgressbarComponent } from "./progressbar/progressbar.component";
import { CommentsComponent } from "./comments/comments.component";
import { GaugeChartComponent } from "./gauge-chart/gauge-chart.component";
import { PieChartComponent } from "./pie-chart/pie-chart.component";
import { RouterModule } from "@angular/router";
import { DatePickerComponent } from "./date-picker/date-picker.component";
import { NgDatePickerComponent } from "./ng2-date-picker/ng-date-picker.component";
// Pipes
import { LocalSearchPipe } from "../../pipes/local-search.pipe";
import { NameTranslatePipe } from "../../pipes/name-translate.pipe";
import { JalaliPipe } from "../../pipes/jalali.pipe";
import { SummeryPipe } from "../../pipes/summery.pipe";
import { splitPipe } from "../../pipes/splitPipe";
import { SplitTitlePipe } from "../../pipes/split-title.pipe";
import { SplitNamePipe } from "../../pipes/split-name.pipe";
import { splitFileName } from "../../pipes/split-file-name.pipe";
import { normalizeString } from "../../pipes/normalize-string.pipe";
import { IssueCodePipe } from "../../pipes/issue-code.pipe";
import { ChartPieComponent } from "./chart-pie/chart-pie.component";
import { TruncatePipe } from "./../../pipes/seprate-words.pipe";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule,
    DpDatePickerModule,
    OrderModule,
    NgxSmartModalModule.forRoot(),
    RouterModule,
  ],
  declarations: [
    CardIcon,
    BreadcrumbComponent,
    PaginationComponent,
    TagInputComponent,
    SelectInputComponent,
    InputItemComponent,
    LocalSearchPipe,
    NameTranslatePipe,
    JalaliPipe,
    TruncatePipe,
    GaugeChartComponent,
    PieChartComponent,
    DatePickerComponent,
    NgDatePickerComponent,
    SummeryPipe,
    SortTableComponent,
    ProgressbarComponent,
    splitPipe,
    SplitTitlePipe,
    SplitNamePipe,
    splitFileName,
    normalizeString,
    IssueCodePipe,
    HeaderSelectInputComponent,
    CommentsComponent,
    ChartPieComponent,
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    OrderModule,
    BreadcrumbComponent,
    CardIcon,
    PaginationComponent,
    TagInputComponent,
    SelectInputComponent,
    InputItemComponent,
    LocalSearchPipe,
    NameTranslatePipe,
    JalaliPipe,
    TruncatePipe,
    GaugeChartComponent,
    PieChartComponent,
    DatePickerComponent,
    NgDatePickerComponent,
    SummeryPipe,
    ProgressbarComponent,
    splitPipe,
    SplitTitlePipe,
    SplitNamePipe,
    splitFileName,
    normalizeString,
    IssueCodePipe,
    HeaderSelectInputComponent,
    CommentsComponent,
    ChartPieComponent,
  ],
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [LocalSearchPipe, JalaliPipe],
    };
  }
}
