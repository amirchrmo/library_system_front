import {
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { NgxSmartModalService } from "ngx-smart-modal";
import * as moment from "moment-jalali";
import * as momentOriginal from "moment";
import { QueryService } from "../../../services/query.service";
import { ApiService } from "../../../services/api.service";
import { LocalStorageService } from "angular-2-local-storage";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

declare let Highcharts: any;
declare let $: any;
@Component({
  selector: "chart-pie",
  templateUrl: "./chart-pie.component.html",
  styles: [],
})
export class ChartPieComponent implements OnInit {
  @ViewChild("chart", { static: true }) chart: ElementRef;

  @Input("title") title: string = "";
  @Input("colors") colors = ["#35fb9b", "#fb7c7c"];
  @Input("uniqueID") uniqueID: string = "";
  @Input("data")
  data = []; /*
  example data for ip pool :
  [
    {
      name: "Free",
      y: 70
    },
    {
      name: "Used",
      y: 30
    },
  ]
  */

  public gaugeOptions = {};

  constructor() {}

  ngOnInit() {
    this.chart.nativeElement.id = this.uniqueID;
    this.createChart(this.uniqueID);
  }

  public createChart(id) {
    Highcharts.chart(
      id,

      Highcharts.merge(this.gaugeOptions, {
        chart: {
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          type: "pie",
        },
        title: {
          text: this.title,
        },

        accessibility: {
          announceNewData: {
            enabled: true,
          },
          point: {
            valueSuffix: "%",
          },
        },
        exporting: {
          enabled: false,
        },
        credits: {
          enabled: false,
        },
        plotOptions: {
          pie: {
            colors: this.colors,
          },
          series: {
            dataLabels: {
              enabled: true,
              format: "{point.name}: {point.y:.1f}%",
            },
          },
        },

        tooltip: {
          headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
          pointFormat:
            '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>',
        },

        series: [
          {
            name: "Browsers",
            colorByPoint: true,
            data: this.data,
          },
        ],
      })
    );
  }
}
