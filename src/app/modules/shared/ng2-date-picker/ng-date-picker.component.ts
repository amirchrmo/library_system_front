import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChange,
  SimpleChanges,
  ViewChild,
} from "@angular/core";
import * as momentNs from "jalali-moment";
import * as momentJalali from "moment-jalali";
import * as originamMoment from "moment";
import { Moment } from "jalali-moment";
import { LocalStorageService } from "angular-2-local-storage";
import { TranslateService } from "@ngx-translate/core";
import { Subject } from "rxjs";
const moment = momentNs;

declare var $: any;
declare var pDatepicker: any;

@Component({
  selector: "ng-date-picker",
  templateUrl: "./ng-date-picker.component.html",
  styles: [],
})
export class NgDatePickerComponent implements OnInit, OnDestroy, OnChanges {
  @ViewChild("dateComponent", { static: true }) dateComponent;
  @Input("date") date: Moment | string;
  @Input("default") default = 0;
  @Input("disabled") disabled: boolean = false;
  @Input("required") required: boolean = false;
  @Input("showPlaceholder") showPlaceholder: boolean = true;
  @Input("mode") mode: "day" | "daytime" = "daytime";
  @Input("placeholder") placeholder: string = "";
  @Input("material") material: boolean = true;
  @Output() onChangeDate: EventEmitter<any> = new EventEmitter();
  @Input("direction") direction: "rtl" | "ltr" = "ltr";

  constructor(
    private _storage: LocalStorageService,
    private _translate: TranslateService
  ) {}
  onChanges = new Subject<SimpleChanges>();
  ngOnChanges(changes: SimpleChanges): void {
    this.onChanges.next(changes);
  }
  displayDate: Moment | string;
  validationMinDate = "";
  validationMaxDate = "";

  jalaliConfigExtension = {
    firstDayOfWeek: "sa",
    monthFormat: "MMMM YY",
    weekDayFormat: "dd",
    dayBtnFormat: "D",
    monthBtnFormat: "MMMM",
    locale: "fa",
    format: "YYYY/MM/DD HH:mm:ss",
  };
  gregorianSystemDefaults = {
    firstDayOfWeek: "su",
    monthFormat: "MMM, YYYY",
    disableKeypress: false,
    allowMultiSelect: false,
    closeOnSelect: undefined,
    closeOnSelectDelay: 100,
    openOnFocus: true,
    openOnClick: true,
    onOpenDelay: 0,
    weekDayFormat: "ddd",
    appendTo: document.body,
    showNearMonthDays: true,
    showWeekNumbers: false,
    enableMonthSelector: true,
    yearFormat: "YYYY",
    showGoToCurrent: true,
    dayBtnFormat: "DD",
    monthBtnFormat: "MMM",
    hours12Format: "hh",
    hours24Format: "HH",
    meridiemFormat: "A",
    minutesFormat: "mm",
    minutesInterval: 1,
    secondsFormat: "ss",
    secondsInterval: 1,
    showSeconds: true,
    showTwentyFourHours: false,
    timeSeparator: ":",
    multipleYearsNavigateBy: 10,
    showMultipleYearsNavigation: false,
    locale: "en",
    hideInputContainer: false,
    unSelectOnClick: true,
    hideOnOutsideClick: true,
    format: "YYYY/MM/DD HH:mm:ss",
  };
  config = { ...this.gregorianSystemDefaults, ...this.jalaliConfigExtension };
  public lang: string = this._storage.get("lang");

  public enFormatDate = "YYYY-MM-DD-HH:mm:ss";
  public faFormatDate = "jYYYY,jMM,jDD HH:mm:ss";
  public onLangChage() {
    this.config.locale = this.lang;
    this.config = { ...this.config };
    this.refreshDemo();
  }
  ngOnInit() {
    this.onLangChage();
    if (this.showPlaceholder) {
      this.placeholder = this._translate.instant("choose-date");
    } else {
      this.placeholder = "";
    }

    this._translate.onLangChange.subscribe((res) => {
      this.lang = res["lang"];
      // this.direction = this.lang === "fa" ? "rtl" : "ltr";
      if (this.showPlaceholder) {
        this.placeholder = this._translate.instant("choose-date");
      } else {
        this.placeholder = "";
      }
      this.onLangChage();
    });
    this.onChanges.subscribe((data: SimpleChanges) => {
      if (this.default !== 0) {
        this.setDate(Number(this.default));
      }
    });
    this.config.format =
      this.mode === "day" ? "YYYY/MM/DD" : "YYYY/MM/DD HH:mm:ss";
    this.refreshDemo();
    if (this.default !== 0) {
      this.setDate(Number(this.default));
    }
  }
  setDate(defaultTime) {
    let date = momentJalali(defaultTime).format("jYYYY-jMM-jDD-HH:mm:ss");
    this.date =
      this.mode === "day"
        ? moment(String(date), "jYYYY-jMM-jDD")
        : moment(String(date), "jYYYY-jMM-jDD-HH:mm:ss");
  }
  showDemo = true;
  refreshDemo() {
    this.showDemo = false;
    setTimeout(() => {
      this.showDemo = true;
    });
  }
  log(event) {
    this.onChangeDate.emit(event? moment(event).toISOString():null);
  }
  opened() {
    // console.log("open");
  }
  closed() {
    // console.log("close");
  }
  onLeftNav(event) {
    // console.log("onLeftNav");
  }
  onRightNav(event) {
    // console.log("onRightNav");
  }
  ngOnDestroy() {
    this.onChanges.complete();
  }
}
