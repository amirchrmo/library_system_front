import {Component, ElementRef, Input, OnChanges, OnInit, ViewChild,} from "@angular/core";
import {TranslateService} from "@ngx-translate/core";

declare let Highcharts: any;

@Component({
  selector: "pie-chart",
  templateUrl: "./pie-chart.component.html",
  styles: [],
})
export class PieChartComponent implements OnInit, OnChanges {
  @ViewChild("chart", {static: true}) chart: ElementRef;

  @Input("data") data: number = 0;
  @Input("desc") desc: object = {titleButton: "", count: 0};
  @Input("uniqueID") uniqueID: string = "";
  @Input("url") url: string[] = ["/"];
  @Input("params") params: {} = {};
  @Input("numberOfCards") numberOfCards: number = 0;
  public chartPie;
  public count = 0;
  public titleButton = "";
  public param = "";
  public newData: Array<any> = [
    [
      ["Open request", 0],
      ["Pending", 0],
      ["Waiting for closure", 0],
      ["Closed 24 hours", 0],
    ],
    [
      ["Open request", 0],
      ["Pending", 0],
      ["Waiting for closure", 0],
      ["Closed 24 hours", 0],
    ],
    [
      ["Open request", 0],
      ["Pending", 0],
      ["Waiting for closure", 0],
      ["Closed 24 hours", 0],
    ],
    [
      ["Open request", 0],
      ["Pending", 0],
      ["Waiting for closure", 0],
      ["Closed 24 hours", 0],
    ],
    [
      ["Open request", 0],
      ["Pending", 0],
      ["Waiting for closure", 0],
      ["Closed 24 hours", 0],
    ],
    [
      ["Open request", 0],
      ["Pending", 0],
      ["Waiting for closure", 0],
      ["Closed 24 hours", 0],
    ],
  ];

  constructor(private _translate: TranslateService) {
  }

  private _valueOfNode: Array<any>;

  get valueOfNode(): Array<any> {
    return this._valueOfNode;
  }

  ngOnInit() {
    // this.count = this.desc['count'];
    // this.titleButton = this.desc['titleButton'];
    // this.param = this.desc['param'];
    // this.chart.nativeElement.id = this.uniqueID;
    // if(this.count) {
    //   this._initChart(this.data, this.uniqueID, this.count);
    // } else
    //   this._initChart(this.data, this.uniqueID, 0);

  }

  ngOnChanges() {
    this.count = this.desc["count"];
    this.titleButton = this.desc["titleButton"];
    this.param = this.desc["param"];
    // this.chart.nativeElement.id = this.uniqueID;
    // if (this.count) {
    //   this._initChart(this.data, this.uniqueID, this.count);
    // } else this._initChart(this.data, this.uniqueID, 0);
  }

  private _initChart(data: Array<any>, id, count?: number): void {
    this.chartPie = Highcharts.chart(id, {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: 0,
        plotShadow: false,
        backgroundColor: "rgba(0,0,0,0)",
      },
      title: {
        text: "<br>" + count.toString() + "</br>",
        align: "center",
        verticalAlign: "middle",
        y: 15,
      },
      tooltip: {
        pointFormat: "{series.name}:{point.y}",
      },
      accessibility: {
        point: {
          valueSuffix: "%",
        },
      },
      credits: {
        enabled: false,
      },
      exporting: {
        enabled: false,
      },
      plotOptions: {
        pie: {
          dataLabels: {
            enabled: false,
            distance: -50,
            style: {
              fontWeight: "bold",
              color: "white",
            },
          },
          startAngle: -180,
          endAngle: 180,
          center: ["50%", "50%"],
          size: "100%",
          colors: ["#FAC89B", "#9BAFC3", "#8C6987", "#B4DCAF"],
        },
      },
      series: [
        {
          type: "pie",
          name: "",
          innerSize: "70%",
          data: data,
        },
      ],
    });
  }
}
