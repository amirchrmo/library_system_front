import { Component, OnInit } from "@angular/core";
import { LocalStorageService } from "angular-2-local-storage";
import { TranslateService } from "@ngx-translate/core";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { ApiService } from "../../services/api.service";

@Component({
  selector: "app-myBooks",
  templateUrl: "./myBooks.component.html",
  styleUrls: [],
})
export class MyBooksComponent implements OnInit {
  public filters = {
    limit: 10,
    name: "",
    page: 1,
    writer: "",
    publisher: "",
    Id: "",
    category: ""
  };
  listBook: any;
  totalPages: number =1;
  totalCount: any =1;
  perPage: any =10;
  userInfo: any;

  constructor(
    private _api: ApiService,
    private _storage: LocalStorageService,
  ) {
  }
  ngOnInit() {
    this.userInfo = this._storage.get("current-user");
    this.getMyBooks(this.userInfo.user.username)
  }


  public getMyBooks(username): void {
    this._api.set("user/access", "POST", {body:{'username': username}, id: "getBooks" }, (res) => {
      this.listBook = res.books || [];
    });
  }
}
