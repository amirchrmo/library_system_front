import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { MyBooksComponent } from './myBooks.component';


const routes: Routes = [
  {
    path: '',
    component: MyBooksComponent,
    data: {
      title: 'MyBooksComponent.title'
    }
  },
  // {
  //   path: "",
  //   redirectTo: "/dashboard",
  //   pathMatch: "full",
  // },
  // {
  //   path: "**",
  //   redirectTo: "/dashboard",
  // },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyBooksRoutingModule {
}
