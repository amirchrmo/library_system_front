import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { MyBooksComponent } from './myBooks.component';
import { MyBooksRoutingModule } from './myBooks-routing.module';

@NgModule({
  imports: [
    CommonModule,
    MyBooksRoutingModule,
    SharedModule,
  ],
  declarations: [
    MyBooksComponent
    ]
})
export class MyBooksModule {
}
