import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { BooksComponent } from './books.component';
import { BooksRoutingModule } from './books-routing.module';

@NgModule({
  imports: [
    CommonModule,
    BooksRoutingModule,
    SharedModule,
  ],
  declarations: [
    BooksComponent
    ]
})
export class BooksModule {
}
