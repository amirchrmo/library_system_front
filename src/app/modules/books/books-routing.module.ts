import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { BooksComponent } from './books.component';


const routes: Routes = [
  {
    path: '',
    component: BooksComponent,
    data: {
      title: 'books.title'
    }
  },
  // {
  //   path: "",
  //   redirectTo: "/dashboard",
  //   pathMatch: "full",
  // },
  // {
  //   path: "**",
  //   redirectTo: "/dashboard",
  // },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BooksRoutingModule {
}
