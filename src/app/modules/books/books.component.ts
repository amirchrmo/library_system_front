import { Component, OnInit } from "@angular/core";
import { LocalStorageService } from "angular-2-local-storage";
import { TranslateService } from "@ngx-translate/core";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { ApiService } from "./../../services/api.service";

@Component({
  selector: "app-books",
  templateUrl: "./books.component.html",
  styleUrls: [],
})
export class BooksComponent implements OnInit {
  public filters = {
    limit: 10,
    name: "",
    page: 1,
    writer: "",
    publisher: "",
    Id: "",
    category: ""
  };
  listBook: any;
  totalPages: number =1;
  totalCount: any =1;
  perPage: any =10;

  constructor(
    private _api: ApiService,
  ) {
  }
  ngOnInit() {
    this.getBooks();
  }


  public getBooks(pageNumber: number = this.filters.page): void {
    this.filters.page = pageNumber;
    this._api.set("user/searchbook", "POST", {body:this.filters, id: "getBooks" }, (res) => {
      this.listBook = res.data || [];
      this.totalPages = Math.ceil(res.totalCountOfData / this.filters.limit);
      this.totalCount = res.totalCountOfData;
    });
  }

  public getBookPerPage(event): void {
    this.filters.limit = event;
    this.perPage = event;
    this.getBooks(1);
  }
}
