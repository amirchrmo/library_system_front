import { Component, OnInit } from "@angular/core";
import { LocalStorageService } from "angular-2-local-storage";
import { TranslateService } from "@ngx-translate/core";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { ApiService } from "../../services/api.service";
import { NgxSmartModalService } from "ngx-smart-modal";
import {Ent, NotifyService, Opr, SwlT,} from "../../services/notify.service";
import { retry } from "rxjs/operators";

@Component({
  selector: "app-loanBook",
  templateUrl: "./loanBook.component.html",
  styleUrls: [],
})
export class LoanBookComponent implements OnInit {
  username: any;
  userInfo: any;
  //
  public filters = {
    limit: 10,
    name: "",
    page: 1,
    writer: "",
    publisher: "",
    Id: "",
    category: ""
  };
  listBook: any;
  totalPages: number =1;
  totalCount: any =1;
  perPage: any =10;
  listBookUser: any;
  constructor(
    private _api: ApiService,
    public modal: NgxSmartModalService,
    private _notify: NotifyService,
  ) {
  }
  ngOnInit() {
    this.getBooks();
  }


  public getUserInfo() {
    this._api.set("user/access", "POST", {body:{'username': this.username}, id: "getUserInfo" }, (res) => {
      this.userInfo = res.User || [];
      this.listBookUser = res.books || [];
    });
  }
  submitBood(item) {
    if(this.listBookUser.length >= 3 || item.status == 'loaned') {
      this._notify.alert(SwlT.Error, 'کاربر شرایط امانت کتاب را ندارد', SwlT.Error);
      return;    
    }
    this._api.set("user/loan", "POST", {body:{'username': this.username, 'bsonid': item.Id}, id: "getUserInfo" }, (res) => {
      this._notify.alert(SwlT.Success, 'ثبت کتاب با موفقیت انجام شد').then(() => {
        this.getUserInfo();
        this.modal.getModal('listBook').close();
      });
    });
  }

  public getBooks(pageNumber: number = this.filters.page): void {
    this.filters.page = pageNumber;
    this._api.set("user/searchbook", "POST", {body:this.filters, id: "getBooks" }, (res) => {
      this.listBook = res.data || [];
      this.totalPages = Math.ceil(res.totalCountOfData / this.filters.limit);
      this.totalCount = res.totalCountOfData;
    });
  }

  public getBookPerPage(event): void {
    this.filters.limit = event;
    this.perPage = event;
    this.getBooks(1);
  }
  
}
