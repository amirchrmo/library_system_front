import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { LoanBookComponent } from './loanBook.component';
import { LoanBookRoutingModule } from './loanBook-routing.module';
import { NgxSmartModalModule } from 'ngx-smart-modal';

@NgModule({
  imports: [
    CommonModule,
    LoanBookRoutingModule,
    SharedModule,
    NgxSmartModalModule,
  ],
  declarations: [
    LoanBookComponent
    ]
})
export class LoanBookModule {
}
