import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { LoanBookComponent } from './loanBook.component';


const routes: Routes = [
  {
    path: '',
    component: LoanBookComponent,
    data: {
      title: 'loanBook.title'
    }
  },
  // {
  //   path: "",
  //   redirectTo: "/dashboard",
  //   pathMatch: "full",
  // },
  // {
  //   path: "**",
  //   redirectTo: "/dashboard",
  // },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoanBookRoutingModule {
}
