import {ApiService} from "./../../../services/api.service";
import {Component, OnInit, Renderer2} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import { TranslateService } from "@ngx-translate/core";

import {UserService} from "../../../services/user.service";

@Component({
  selector: "auth-login",
  templateUrl: "./login.component.html",
  styles: [],
})
export class LoginComponent implements OnInit {
  public interval: any;
  public headers;
  public captchaID;
  public captchaResult;
  public imageSrc: any;
  public form: FormGroup;
  public checked: boolean = this.translate.currentLang == 'fa';

  constructor(
    private _renderer: Renderer2,
    private _fb: FormBuilder,
    public translate: TranslateService,
    private _userService: UserService,
    private _api: ApiService,
    private http: HttpClient
  ) {
    document.addEventListener("visibilitychange", () => {
      if (document.visibilityState == "visible") {
        // this.generateCaptcha();
      }
    }, false)
    // this.generateCaptcha();
    this._migrateForm();
    this._renderer.addClass(document.body, 'auth-overlay');
  }

  public generateCaptcha() {
    this._api.set("captcha", "GET", {params: {w: 355, h: 110}}, (res) => {
      this.imageSrc = res.captcha;
      this.imageSrc = "data:image/png;base64," + this.imageSrc;
      this.captchaID = res.id;

      this.form.get("captcha_id").patchValue(this.captchaID);
      this.form.get("captcha_challenge").patchValue(this.captchaResult);
    });
  }

  ngOnInit() {
  }

  public submit(values: any) {
    let error = this._userService.login(values);
    // if (error == undefined) {
    //   this.generateCaptcha();
    // }
  }

  ngOnDestroy() {
    this._renderer.removeClass(document.body, 'auth-overlay');
  }

  private _migrateForm(): void {
    this.form = this._fb.group({
      username: ["", Validators.compose([Validators.required])],
      password: [
        "",
        Validators.compose([
          Validators.required,
          Validators.minLength(6)
        ]),
      ],
      // captcha_id: [""],
      // captcha_challenge: ["", Validators.compose([Validators.required])],
    });
  }

  public toggleLanguage(event) {
    this.translate.use(this.translate.currentLang == 'en' ? 'fa' : 'en').subscribe(() => {
    });
  }
}
