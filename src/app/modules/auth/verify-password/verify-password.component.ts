import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { TranslateService } from "@ngx-translate/core";
import { NotificationsService } from "angular2-notifications";

import { ApiService } from "../../../services/api.service";
import { CustomValidators } from "../../../validators/custom-validators";
import { HttpErrorResponse } from "@angular/common/http";
import { Router } from "@angular/router";
import { GeneralVariable } from "../../../../general-variable";
import { UserService } from "../../../../app/services/user.service";

@Component({
  selector: "app-verify-password",
  templateUrl: "./verify-password.component.html",
  styles: [],
})
export class VerifyPassword implements OnInit {
  public checked: boolean = this.translate.currentLang == "fa";

  // public formShow = this.cellphone;
  public passInputType: "password" | "text" = "password";
  public form: FormGroup;
  public formVerify: FormGroup;
  public formResetPass: FormGroup;
  public formSubmited: boolean = false;
  public captchaID;
  public captchaResult;
  public imageSrc: any;
  constructor(
    private _fb: FormBuilder,
    public translate: TranslateService,
    private _notify: NotificationsService,
    private _api: ApiService,
    public generalVariable: GeneralVariable,
    private _router: Router,
    private _userService: UserService
  ) {}

  ngOnInit() {
    this._migrateForm();
    this.generateCaptcha();
  }
  public generateCaptcha() {
    this._api.set("captcha", "GET", { params: { w: 355, h: 110 } }, (res) => {
      this.imageSrc = res.captcha;
      this.imageSrc = "data:image/png;base64," + this.imageSrc;
      this.captchaID = res.id;

      this.form.get("captcha_id").patchValue(this.captchaID);
      this.form.get("captcha_challenge").patchValue(this.captchaResult);
    });
  }
  public togglePasswordVisibility(): void {
    this.passInputType = this.passInputType == "password" ? "text" : "password";
  }
  public submit(form) {
    this.formSubmited = true;
    if (this.form.invalid) return;
    this._api.set(
      "auth/otp/confirm",
      "POST",
      {
        body: this.form.value,
      },
      (res) => {
        this._userService.setUserStorage(res);
        this._userService.updateUserStorage();
        this._router.navigate(["/auth/password/reset"], {
          queryParams: { id: res.user.id },
        });
      },
      () => {
        this.generateCaptcha();
      }
    );
  }

  public toggleLanguage(event) {
    this.translate
      .use(this.translate.currentLang == "en" ? "fa" : "en")
      .subscribe(() => {});
  }

  ngOnDestroy() {
    this._api.remove("auth/forget_password");
  }

  private _migrateForm(): void {
    this.form = this._fb.group({
      username: ["", Validators.compose([Validators.required])],
      captcha_challenge: ["", Validators.compose([Validators.required])],
      captcha_id: [""],
      code: ["", Validators.compose([Validators.required])],
    });
  }
}
