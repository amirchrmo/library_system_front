import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { ApiService } from "../../../services/api.service";
import { UserService } from "../../../services/user.service";
import { Ent, NotifyService, Opr } from "../../../services/notify.service";
import { Router } from "@angular/router";

import { UserStorage } from "../../../interfaces/user-storage";
import { GeneralVariable } from "../../../../general-variable";
import { LocalStorageService } from "angular-2-local-storage";

@Component({
  selector: 'auth-register',
  templateUrl: './register.component.html',
  styles: []
})
export class RegisterComponent implements OnInit {

  public form: FormGroup;
  public formSubmitted: boolean = false;

  constructor(
    private _fb: FormBuilder,
    private _api: ApiService,
    private _router: Router,
    private _userService: UserService,
    private _notify: NotifyService,
    private _storage: LocalStorageService,
    public generalVariable: GeneralVariable
  ) {
    this._migrateForm();
  }

  ngOnInit() {

  }

  public submit(form: FormGroup) {
    this.formSubmitted = true;
    if (form.invalid) return;
    this._api.set(
      "user/register",
      "POST",
      {
        body: form.value,
      },
      (res): void => {
        // TODO: We can Use "this._userService.updateUserStorage()" instead of following lines
        this._notify.status(Opr.Edit, Ent.UserProfile);
        this._router.navigate(["/auth"]);
      }
    );
  }

  ngOnDestroy() {
  }

  private _migrateForm(): void {
    this.form = this._fb.group({
      userName: ["", Validators.required],
      password: ["", Validators.required],
      email: ["", Validators.required],
      fullName: ["", Validators.required],
    });
  }
}
