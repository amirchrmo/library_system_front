import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { AuthRoutingModule } from "./auth-routing.module";
import { SharedModule } from "../../modules/shared/shared.module";

import { AuthComponent } from "./auth.component";
import { LoginComponent } from "./login/login.component";
import { RegisterComponent } from "./register/register.component";
import { ForgetPasswordComponent } from "./forget-password/forget-password.component";
import { ResetPasswordComponent } from "./reset-password/reset-password.component";
import { UserProfileComponent } from "./user-profile/user-profile.component";
import { VerifyPassword } from "./verify-password/verify-password.component";

@NgModule({
  imports: [CommonModule, AuthRoutingModule, SharedModule],
  declarations: [
    AuthComponent,
    LoginComponent,
    RegisterComponent,
    ForgetPasswordComponent,
    ResetPasswordComponent,
    UserProfileComponent,
    VerifyPassword,
  ],
})
export class AuthModule {}
