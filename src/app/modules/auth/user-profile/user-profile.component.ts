import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { ApiService } from "../../../services/api.service";
import { UserService } from "../../../services/user.service";
import { Ent, NotifyService, Opr } from "../../../services/notify.service";
import { Router } from "@angular/router";

import { UserStorage } from "../../../interfaces/user-storage";
import { GeneralVariable } from "../../../../general-variable";
import { LocalStorageService } from "angular-2-local-storage";

@Component({
  selector: "app-user-profile",
  templateUrl: "./user-profile.component.html",
  styles: [],
})
export class UserProfileComponent implements OnInit {
  public userId;
  public userInfo;
  public userRoles;
  public form: FormGroup;
  public formSubmitted: boolean = false;
  user: any;

  constructor(
    private _fb: FormBuilder,
    private _router: Router,
    private _api: ApiService,
    private _userService: UserService,
    private _notify: NotifyService,
    private _storage: LocalStorageService,
    public generalVariable: GeneralVariable
  ) {
    this.userInfo = this._storage.get("current-user");
    // let userInfo = Object.values(user);
    // this.userInfo = userInfo[0];
    // let userId = userInfo[0].id;
    // this.userId = userId;
    // this.userRoles = this.userInfo["roles"];

    // this._migrateForm();
    // this._getUser(this.userInfo);
  }

  ngOnInit() {

  }

  // public submit(form: FormGroup) {
  //   this.formSubmitted = true;
  //   if (form.invalid) return;
  //   this.form.removeControl("username");
  //   this._api.set(
  //     "user/current",
  //     "PUT",
  //     {
  //       body: form.value,
  //     },
  //     (res: { user: ApiModel.User }): void => {
  //       // TODO: We can Use "this._userService.updateUserStorage()" instead of following lines
  //       let userStorage: UserStorage = this._userService.userStorage();
  //       userStorage.user = res.user;
  //       this._userService.setUserStorage(userStorage);
  //       this._notify.status(Opr.Edit, Ent.UserProfile);
  //       this._router.navigate(["/main"]);
  //     }
  //   );
  // }

  ngOnDestroy() {
    this._api.remove("user");
    this._api.remove("user/profile");
  }

  // private _migrateForm(): void {
  //   this.form = this._fb.group({
  //     username: "",
  //     name: ["", Validators.required],
  //     email: "",
  //     cellphone: ["", Validators.required],
  //   });
  // }

  // private _getUser(user): void {
  //   // this.form.patchValue(this.userInfo);
  //   this._api.set(
  //         "user/access",
  //         "PUT",
  //         {
  //           body: user.user.username,
  //         },
  //         (res: { user: ApiModel.User }): void => {
  //           // TODO: We can Use "this._userService.updateUserStorage()" instead of following lines
  //         }
  //       );
  //     }
}
