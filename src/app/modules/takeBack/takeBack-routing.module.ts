import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { TakeBackComponent } from './takeBack.component';


const routes: Routes = [
  {
    path: '',
    component: TakeBackComponent,
    data: {
      title: 'takeBack.title'
    }
  },
  // {
  //   path: "",
  //   redirectTo: "/dashboard",
  //   pathMatch: "full",
  // },
  // {
  //   path: "**",
  //   redirectTo: "/dashboard",
  // },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TakeBackRoutingModule {
}
