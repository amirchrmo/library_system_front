import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { TakeBackComponent } from './takeBack.component';
import { TakeBackRoutingModule } from './takeBack-routing.module';
import { NgxSmartModalModule } from 'ngx-smart-modal';

@NgModule({
  imports: [
    CommonModule,
    TakeBackRoutingModule,
    SharedModule,
    NgxSmartModalModule,
  ],
  declarations: [
    TakeBackComponent
    ]
})
export class TakeBackModule {
}
