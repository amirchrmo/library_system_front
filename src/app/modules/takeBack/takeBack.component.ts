import { Component, OnInit } from "@angular/core";
import { LocalStorageService } from "angular-2-local-storage";
import { TranslateService } from "@ngx-translate/core";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { ApiService } from "../../services/api.service";
import { NgxSmartModalService } from "ngx-smart-modal";
import {Ent, NotifyService, Opr, SwlT,} from "../../services/notify.service";

@Component({
  selector: "app-takeBack",
  templateUrl: "./takeBack.component.html",
  styleUrls: [],
})
export class TakeBackComponent implements OnInit {
  username: any;
  userInfo: any;
  //
  public filters = {
    limit: 10,
    name: "",
    page: 1,
    writer: "",
    publisher: "",
    Id: "",
    category: ""
  };
  listBook: any;
  totalPages: number =1;
  totalCount: any =1;
  perPage: any =10;
  listBookUser: any;
  constructor(
    private _api: ApiService,
    public modal: NgxSmartModalService,
    private _notify: NotifyService,
  ) {
  }
  ngOnInit() {
  }


  public getUserInfo() {
    this._api.set("user/access", "POST", {body:{'username': this.username}, id: "getUserInfo" }, (res) => {
      this.userInfo = res.User || [];
      this.listBookUser = res.books || [];
    });
  }
  giveBackBook(item) {
    this._notify.confirm(Opr.Select, Ent.DynamicReport, () => {
      this._api.set("user/takeback", "POST", {body:{'bsonid': item.Id}}, (res) => {
        this.getUserInfo();
        this._notify.status(Opr.Select, Ent.DynamicReport);
      });
    });
  } 
}
