import { Injectable, Injector } from "@angular/core";
import {
  HttpEvent,
  HttpHandler,
  HttpHeaders,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
} from "@angular/common/http";
import { Observable, throwError as observableThrowError } from "rxjs";
import { catchError, map } from "rxjs/operators";

import { NotificationsService } from "angular2-notifications";
import { TranslateService } from "@ngx-translate/core";

import { UserService } from "./services/user.service";
import { Router } from "@angular/router";

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
  private _userService: UserService;
  private _translate: TranslateService;

  constructor(
    private inj: Injector,
    private _notify: NotificationsService,
    private router: Router
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    this._userService = this.inj.get(UserService);
    this._translate = this.inj.get(TranslateService);

    if (!req.url.includes("static"))
      req = req.clone({
        url: `./api/v1/${req.url}`,
      });

    let userToken: string = this._userService.userStorage(true);
    let lang = "fa";
    lang = this._userService.getLang();

    if (userToken) {
      req = req.clone({
        setHeaders: {
          Authorization: `Bearer ${userToken}`,
        },
      });
    }

    req = req.clone({
      setHeaders: {
        language: lang,
      },
    });

    return next.handle(req).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) return event;
      }),
      catchError((err) => {
        switch (err.status) {
          case 400:
            // switch (err.error["error"]) {
            //   case "http: no such file":
            //     this._translate.get("notify.no-file").subscribe((text) => {
            //       this._notify.error(text.title, text.message);
            //     });
            //     break;
            //   case "username or password not matched":
            //     this._translate.get("notify.login-error").subscribe((text) => {
            //       this._notify.error(text.title, text.message);
            //     });
            //     break;
            //   case "captcha not matched":
            //     this._translate
            //       .get("notify.captcha-error")
            //       .subscribe((text) => {
            //         this._notify.error(text.title, text.message);
            //       });
            //     break;
            //   case "you can't login in this panel":
            //     this._translate
            //       .get("notify.super-user-error")
            //       .subscribe((text) => {
            //         this._notify.error(text.title, text.message);
            //       });
            //     break;
            //   case "duplicate parent name":
            //     this._translate
            //       .get("notify.duplicate_parent_name")
            //       .subscribe((text) => {
            //         this._notify.error(text.title, text.message);
            //       });
            //     break;
            //   case "error in appending child":
            //     this._translate
            //       .get("notify.duplicate_child_name")
            //       .subscribe((text) => {
            //         this._notify.error(text.title, text.message);
            //       });
            //     break;
            //   case "missing or malformed jwt":
            //     this._userService.ejectUser({ notify: false });
            //     break;

            //   default:
            //     if (err.error["message"] == "missing or malformed jwt") {
            //       this._userService.ejectUser({ notify: false });
            //     } else {
            //       this._translate.get("error.400").subscribe((notify) => {
            //         this._notify.error(notify.title, notify.message);
            //       });
            //     }
            //     break;
            // }
            if (err.error["error"] != undefined) {
              this._notify.error(err.error["error"]);
            }

            break;
          case 401:
            // this._translate.get("error.401").subscribe(notify => {
            //   this._notify.error(notify.title, notify.message);
            // });
            this._notify.error(err.error["error"]);
            // this.router.navigate(["/"]);
            this._userService.ejectUser({ notify: true });
            break;
          case 403:
            // this._translate.get("error.403").subscribe((notify) => {
            //   this._notify.error(notify.title, notify.message);
            // });
            this._notify.error(err.error["error"]);
            // this._userService.ejectUser({ notify: true });
            break;
          case 404:
            // this._translate.get("error.404").subscribe((notify) => {
            //   this._notify.error(notify.title, notify.message);
            // });
            this._notify.error(err.error["error"]);
            break;
          case 500:
            this._translate.get("error.500").subscribe((notify) => {
              this._notify.error(notify.title, notify.message);
            });
            // this._notify.error(err.error["error"]);
            break;
          case 501:
            this._translate.get("error.500").subscribe((notify) => {
              this._notify.error(notify.title, notify.message);
            });
            // this._notify.error(err.error["error"]);
            break;
          case 502:
            this._translate.get("error.500").subscribe((notify) => {
              this._notify.error(notify.title, notify.message);
            });
            // this._notify.error(err.error["error"]);
            break;
          case 503:
            this._translate.get("error.500").subscribe((notify) => {
              this._notify.error(notify.title, notify.message);
            });
            // this._notify.error(err.error["error"]);
            break;
          case 504:
            this._translate.get("error.500").subscribe((notify) => {
              this._notify.error(notify.title, notify.message);
            });
            // this._notify.error(err.error["error"]);
            break;
          case 505:
            this._translate.get("error.500").subscribe((notify) => {
              this._notify.error(notify.title, notify.message);
            });
            // this._notify.error(err.error["error"]);
            break;
          case 506:
            this._translate.get("error.500").subscribe((notify) => {
              this._notify.error(notify.title, notify.message);
            });
            // this._notify.error(err.error["error"]);
            break;
          case 507:
            this._translate.get("error.500").subscribe((notify) => {
              this._notify.error(notify.title, notify.message);
            });
            // this._notify.error(err.error["error"]);
            break;
          case 508:
            this._translate.get("error.500").subscribe((notify) => {
              this._notify.error(notify.title, notify.message);
            });
            // this._notify.error(err.error["error"]);
            break;
          case 510:
            this._translate.get("error.500").subscribe((notify) => {
              this._notify.error(notify.title, notify.message);
            });
            // this._notify.error(err.error["error"]);
            break;
          case 511:
            this._translate.get("error.500").subscribe((notify) => {
              this._notify.error(notify.title, notify.message);
            });
            // this._notify.error(err.error["error"]);
            break;
          default:
            if(err.status){
              this._translate.get("error.network_problem").subscribe((notify) => {
                this._notify.error(notify.title, notify.message);
              });
            }

            break;
        }
        return observableThrowError(err);
      })
    );
  }
}
