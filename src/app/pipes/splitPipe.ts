import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'split'
})
export class splitPipe implements PipeTransform {

  transform(value: string, splitSign: string = "-", index = 0): string {
    if (value == null) return;
    let newVal = value[0].replace(/ *\([^)]*\) */g, '');
    return this.titleCase(newVal, splitSign, index);
  }
  titleCase(str, splitSign, index) {
    var splitStr = str.toLowerCase().split(splitSign);
    for (var i = 0; i < splitStr.length; i++) {
      splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    return splitStr[index]
  }
}