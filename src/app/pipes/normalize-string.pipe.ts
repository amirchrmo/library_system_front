import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "normalizeString",
})
export class normalizeString implements PipeTransform {
  transform(value: string): any {
    if (!value) {
      return null;
    }
    let newValue = value.replace(/[\[\]']+/g, "");
    return newValue;
  }
}
