import { Pipe, PipeTransform } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';

@Pipe({
  name: 'issueCode'
})
export class IssueCodePipe implements PipeTransform {

  public types = [];
  constructor(
    private _storage: LocalStorageService
  ) {
    this.types = this._storage.get("ticket-request-type");
  }

  transform(value: any, field: string = "name"): any {
    this.types = this._storage.get("ticket-request-type");
    if (!this.types) {
      this.types = this._storage.get("ticket-request-type");
    }
    let issue = this.types && this.types.find(t => t.value1 == value);
    if (issue)
      return issue[field];
    else
      return null;
  }

}
