import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'truncate'
})
export class TruncatePipe implements PipeTransform {
  transform(value: string, limit = 2, lang = 'fa', ellipsis = '...') {
    if(lang == 'fa') {
      if(value.split(" ").length > limit) {
        let val = value.split(" ");
        let str="";
        for (let index = 0; index < limit; index++) {
          str += val[index] + ' ';
        }
        return  ellipsis + str;
      } else {
        return value;
      }
    } else {
      if(value.split(" ").length > limit) {
        let val = value.split(" ");
        let str="";
        for (let index = 0; index < limit; index++) {
          str += val[index] + ' ';
        }
        return str + ellipsis;
      } else {
        return value;
      }
    }
  }
}