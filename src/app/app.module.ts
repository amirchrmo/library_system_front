// Built in
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpModule } from "@angular/http";
import {
  HTTP_INTERCEPTORS,
  HttpClient,
  HttpClientModule,
} from "@angular/common/http";
// Third party
// import { NgProgressModule, NgProgressInterceptor } from 'ngx-progressbar';
import { LocalStorageModule } from "angular-2-local-storage";
import { SimpleNotificationsModule } from "angular2-notifications";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
// Interceptor
import { ApiInterceptor } from "./api.interceptor";
// Modules
import { AppRoutingModule } from "./app-routing.module";
import { SharedModule } from "./modules/shared/shared.module";
// Components
import { AppComponent } from "./components/app.component";
import { HeaderComponent } from "./components/header/header.component";
import { SidebarComponent } from "./components/sidebar/sidebar.component";
// Services
import { ApiService } from "./services/api.service";
import { UserService } from "./services/user.service";
import { NotifyService } from "./services/notify.service";
import { SearchService } from "./services/search.service";
import { StorageService } from "./services/storage.service";
import { UrlHelperService } from "./services/url.helper.service";
// Guards
import { AuthGuard } from "./guards/auth.guard";
import { AdminAccessGuard } from "./guards/admin-access.guard";
import { NotificationComponent } from "./components/notification/notification.component";
import { NgxSmartModalModule } from "ngx-smart-modal";
import { HighchartsChartModule } from "highcharts-angular";

import { GeneralVariable } from "../general-variable";
import { carServices } from "./services/car.service";

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, "./statics/i18n/", ".json");
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    NotificationComponent,
  ],
  imports: [
    BrowserModule,
    SharedModule.forRoot(),
    BrowserAnimationsModule,
    SimpleNotificationsModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    AppRoutingModule,
    HttpModule,
    HttpClientModule,
    // NgProgressModule,
    LocalStorageModule.forRoot({
      prefix: "backpanel",
      storageType: "localStorage",
    }),
    HighchartsChartModule,
    NgxSmartModalModule,
  ],
  providers: [
    carServices,
    ApiService,
    UserService,
    NotifyService,
    SearchService,
    StorageService,
    AuthGuard,
    AdminAccessGuard,
    GeneralVariable,
    UrlHelperService,
    { provide: HTTP_INTERCEPTORS, useClass: ApiInterceptor, multi: true },
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: NgProgressInterceptor,
    //   multi: true,
    // },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
