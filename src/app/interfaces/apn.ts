namespace ApiModel {
  export interface Apn {
    id: string;
    confirmed: boolean;
    name: string;
    created_by: string;
    status: boolean;
    type: string;
  }
}
