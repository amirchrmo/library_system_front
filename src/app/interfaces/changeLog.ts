namespace ApiModel {
  export interface ChangeLog {
    field: string;
    fieldtype: string;
    from: string;
    fromString: string;
    to: string;
    toString: string;
  }
}
