namespace OptionsModel {
  export interface CkEditor {
    contentsLangDirection?: string;
    language?: string;
    removePlugins?: string;
    disableNativeSpellChecker?: boolean;
  }
}
