namespace ApiModel {

    export interface Event {
        name: string;
        created: string;
        id: string;
        entity: string;
        entity_id: string;
        action: string;
        formula: string;
        message: string;
        message_email: string;
        notifies: Notifies[];
        enabled: boolean;
    }

    export interface Notifies {
        users: string[];
        usersName?: string[];
        roles: string[];
        rolesName?: string[];
        sms: boolean;
        email: boolean;
        owner_customer: boolean;
    }

    export interface directNotifies {
        name: string;
        users: string[];
        sms: boolean;
        email: boolean;
        message: string;
        message_email: string;
    }

}