namespace ApiModel {
  export interface Acs {
    id: string;
    name: string;
    action: string;
    pre_sync_required: boolean;
    post_sync_required: boolean;
    cpe_models: string[];
    xml: string;
    pre_actions: string[];
    post_actions: string[];
    validators: string[];
    variables: string[];
    access_roles: string[];
    information_template: string;
    information_fields: string[];
  }
}
