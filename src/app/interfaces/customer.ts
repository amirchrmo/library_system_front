namespace ApiModel {
  export interface Customer {
    apn: string;
    contract: Contract;
    coordinators: Coordinator[];
    cpe_lan: string;
    cpe_wan: string;
    created: string;
    customer_side_location: CustomerLocation;
    id: string;
    links: MainLink[];
    mtn_side_location: CustomerLocation;
    name: string;
    nikname: string;
    status: string;
    tunnels: Tunnel[];
    center_address: string;
    services_model: ServiceModel[];
    sla: Sla;
    description: string;
    bandwidth: LocationModel;
    number_of_nodes: number;
  }
  export interface Sla {
    asign_user: String;
    condition_type: String;
    could_solve_duration: Number;
    id: String;
    level: String;
    level_num: Number;
    name: String;
    tags: any;
    value: any;
  }
  export interface ServiceModel {
    id: string;
    name: string;
  }

  export interface Contract {
    start: string;
    end: string;
  }

  export interface Coordinator {
    name: string;
    phone: string;
    mobile: string;
    email: string;
  }

  export interface CustomerLocation {
    city: LocationModel;
    region: LocationModel;
    address: string;
    latitude: string;
    longitude: string;
  }
  export interface LocationModel {
    id: string;
    name: string;
  }

  export interface Tunnel {
    id: string;
    apn: Apn;
    customer_side_ip: string;
    mtn_side_ip: string;
    type: string;
  }
  export interface MainLink {
    id: string;
    name: string;
    title: string;
    bandwidth: {
      name: string;
      id: string;
    };
    apn: {
      name: string;
      id: string;
    };
    customer_side_ip: string;
    mtn_side_ip: string;
    type: {
      name: string;
      id: string;
    };
    customer_side_location: CustomerLocation;
    mtn_side_location: CustomerLocation;
    interface: InterfaceModel;
  }
  export interface InterfaceModel {
    name: string;
    id: string;
    interface_name: string;
  }
}
