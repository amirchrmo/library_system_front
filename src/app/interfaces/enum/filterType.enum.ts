export enum FilterTypeEnum {
    String = 'String', 
    Number = 'Number', 
    Boolean = 'Boolean',
    Datetime = 'Datetime',
    BaseInfo = 'Coding',
    Entity = 'ObjectId'
}
