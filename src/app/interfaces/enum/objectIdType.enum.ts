export enum ObjectIdTypeEnum {
    APN = 'APN', 
    City = 'City', 
    Province = 'Province',
    Customer = 'Customer',
    KPI = 'KPI',
    Node = 'Node',
    Group = 'Group',
    User = 'User',
    Role = 'Role'
}
