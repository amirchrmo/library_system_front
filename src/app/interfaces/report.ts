namespace ApiModel {
    export interface Report {
      access_roles: any[];
      created: string;
      entity: string;
      fields: Object;
      filters: reportFilters[];
      id: string;
      name: string;
      fa_name: string;
      description: string;
      fa_description: string;
      export_note: string;
      export_note_fa: string;
      type: string;
      fields_sort: string[];
      auto_send_schedule: Object;
    }

    export interface reportFilters {
      field: string;
      name: string;
      fa_name: string;
      value: any;
      operator: string;
      type: string;
      meta?: MetaFilter;
      is_primary: boolean;
    }

    export interface reportField {
      name: string;
      titleFA: any;
      titleEN: any;
    }

    export interface reportExport {
      type: string;
      persian: boolean;
      filters: filterExport[];
    }
    
    export interface filterExport {
      id: string;
      value: any;
      is_primary?: boolean;
    }
    export interface MetaFilter {
      meta: string;
      parentValue: any;
      filterValue: Object[];
      provinceList: Object[];
    }
    export interface selectedFilters {
      name:string;
      value: any;
    }

  }
  
 