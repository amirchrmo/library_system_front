namespace ApiModel {
  export interface IPPools {
    AvailableList: [];
    UsedList: [];
    available: number;
    dynamic: boolean;
    from: string;
    lan: boolean;
    netmask: number;
    to: string;
    used: number;
  }
}
