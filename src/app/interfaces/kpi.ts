namespace ApiModel {
  export interface Kpi {
    id: string;
    name: string;
    created: string;
    icmp: boolean;
    oid: string;
    snmp: boolean;
    ssh: boolean;
    type: string;
    value_unit: string;
    value_type: string;
    vendor: string;
  }
}
