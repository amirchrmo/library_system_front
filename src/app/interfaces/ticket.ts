namespace ApiModel {
  export interface Ticket {
    name: string;
    assignee: string;
    created: string;
    customer: string;
    description: string;
    id: string;
    ip: string;
    wanip: string;
    issue_id: string;
    issue_type: string;
    labels: Array<any>;
    meta: any;
    new_assignee_update: boolean;
    new_reporter_update: boolean;
    priority: number;
    ref: string;
    reporter: string;
    status: string;
    title: string;
    updated: string;
    summery: string;
    node: string;
    source: string;
    root_cause: string;
    link: MainLink;
    coordinator: Coordinator;
    informed: Coordinator;
    icare_id: string;
    tunnel: Tunnel;
  }

  export interface Coordinator {
    fullname: string;
    cellphone: string;
    email: string;
  }
}
