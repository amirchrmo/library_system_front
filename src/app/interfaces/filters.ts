namespace ApiModel {
  export interface Filters {
    q: string;
    page: number;
    limit: number;
    status?: string;
    sort?: string;
    issue_type?: string;
    issue_types?: string[];
    assigned?: string;
  }
}
