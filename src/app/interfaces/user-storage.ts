export interface UserStorage {
  user: ApiModel.User,
  token?: string,
}

export interface LoginResult extends UserStorage {
  msg?: string,
  auth_Url?: string,
  redirect_url?: string
}
