namespace ApiModel {
  export interface EnableInterfaces {
    interface_name: string;
    interface_index: string;
    inKpi: string;
    outKpi: string;
    inOct: Number;
    outOct: Number;
    total: Number;
  }
}
