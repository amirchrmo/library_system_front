namespace ApiModel {
  export interface User {
    avatar: string;
    cellphone: string;
    email: string;
    fullname: string;
    gender: string;
    id: string;
    is_admin: boolean;
    martial: string;
    username: string;
    name: string;
    suspend: boolean;
    super_user: boolean;
    enable: boolean;
    roles: Role[];
  }

  export interface Role {
    id: string;
    name: string;
    customer: Customer;
    groups?: Array<Groups>;
  }

  export interface Groups {
    id: string;
    name: string;
    features: Array<Features>;
  }

  export interface Features {
    id: string;
    name: string;
    actions?: Array<string>;
  }
}
