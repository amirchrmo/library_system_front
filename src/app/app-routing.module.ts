
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { AuthGuard } from "./guards/auth.guard";
const routes: Routes = [
  // Dashboard
  {
    path: "dashboard",
    loadChildren: () =>
      import("app/modules/dashboard/dashboard.module").then(
        (m) => m.DashboardModule
      ),
    canActivate: [AuthGuard],
  },
  {
    path: "books",
    loadChildren: () =>
      import("app/modules/books/books.module").then(
        (m) => m.BooksModule
      ),
    canActivate: [AuthGuard],
  },
  {
    path: "loanBook",
    loadChildren: () =>
      import("app/modules/loanBook/loanBook.module").then(
        (m) => m.LoanBookModule
      ),
    canActivate: [AuthGuard],
  },
  {
    path: "takeBack",
    loadChildren: () =>
      import("app/modules/takeBack/takeBack.module").then(
        (m) => m.TakeBackModule
      ),
    canActivate: [AuthGuard],
  },
  {
    path: "myBooks",
    loadChildren: () =>
      import("app/modules/myBooks/myBooks.module").then(
        (m) => m.MyBooksModule
      ),
    canActivate: [AuthGuard],
  },
  // cars list
  // {
  //   path: "car",
  //   loadChildren: () =>
  //     import("app/modules/cars/cars.module").then(
  //       (m) => m.Carmoduls
  //     ),
  // },
  // Auth
  {
    path: "auth",
    loadChildren: () =>
      import("app/modules/auth/auth.module").then(
        (m) => m.AuthModule),
  },
  // Other
  {
    path: "",
    redirectTo: "/auth",
    pathMatch: "full",
  },
  {
    path: "**",
    redirectTo: "/auth",
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: false })],
  exports: [RouterModule],
  providers: [],
})
export class AppRoutingModule {}
