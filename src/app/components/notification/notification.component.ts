import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";

import {ApiService} from "../../services/api.service";


interface getNotification {
  notfications: Notification[],
}

@Component({
  selector: 'notification',
  templateUrl: './notification.component.html',
  styles: []
})

export class NotificationComponent implements OnInit {

  public notifications: Notification[] = [];
  public section: string = "";
  public link: any = '';
  public timer: any;

  constructor(
    private _router: Router,
    private _api: ApiService,
  ) {
    this.getNotifications();
    this.getNotificationByInterval();
  }

  public getNotifications() {
    this._api.set("notification", "GET", {id: "getNotifications"}, (res: getNotification): void => {
      this.notifications = res.notfications;
    });
  }

  public readNotification(notificationLink: any = '', id: string = '') {
    this.link = notificationLink;
    this.link = this.link.replace('/admin', '');
    this._router.navigate([this.link]);
    this._api.set(`notification/read/${id}`, "PATCH", {id: "readNotification"}, (): void => {
      this.getNotifications();
    });
  }

  public markAllAsRead() {
    this._api.set("notification/read/all", "PATCH", {id: "markAllAsRead"}, (): void => {
      this.getNotifications();
    });
  }

  public getNotificationByInterval() {
    this.timer = setInterval(() => {
      this.getNotifications();
    }, 10000);
    return this.timer;
  }

  public getNotificationsByClick() {
    this.getNotifications();
    clearInterval(this.timer);
    this.timer = 0;
    this.timer = setInterval(() => {
      this.getNotifications();
    }, 10000)
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this._api.remove("getNotifications");
    this._api.remove("readNotification");
    this._api.remove("markAllAsRead");
  }
}
