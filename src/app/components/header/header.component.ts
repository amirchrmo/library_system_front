import {AfterViewInit, Component, EventEmitter, OnInit, Output} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

import {UserService} from '../../services/user.service';

declare let $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: []
})
export class HeaderComponent implements OnInit, AfterViewInit {

  @Output() public sidebarToggle: EventEmitter<void> = new EventEmitter<void>();
  public checked: boolean = this._translate.currentLang == 'fa';

  constructor(
    private _translate: TranslateService,
    public userService: UserService
  ) {
  }

  ngOnInit() {
  }

  public toggleLanguage(event) {
    this._translate.use(event.target.checked ? 'fa' : 'en').subscribe(() => {
    });
  }

  public logout(event): void {
    event.preventDefault();
    this.userService.logout();
  }

  ngAfterViewInit(): void {
  }
}
