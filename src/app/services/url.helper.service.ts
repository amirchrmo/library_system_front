import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions, ResponseContentType} from '@angular/http';
import {Observable, Subscriber} from 'rxjs';

@Injectable()
export class UrlHelperService {
  constructor(private http: Http) {
  }

  get(url: string): Observable<any> {
    let options = new RequestOptions();
    options.headers = new Headers();
    let token = localStorage.getItem("backpanel.token")
    token = token.replace(/["]+/g, '');
    options.headers.append("Authorization", `Bearer ${token}`);
    options.responseType = ResponseContentType.Blob;

    return new Observable((observer: Subscriber<any>) => {
      let objectUrl: string = null;

      this.http
        .get(url, options)
        .subscribe(m => {
          objectUrl = URL.createObjectURL(m.blob());
          observer.next(objectUrl);
        });

      return () => {
        if (objectUrl) {
          URL.revokeObjectURL(objectUrl);
        }
      };
    });
  }
}
