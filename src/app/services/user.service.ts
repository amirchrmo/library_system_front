import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";

import { LocalStorageService } from "angular-2-local-storage";
import { NotificationsService } from "angular2-notifications";
import { TranslateService } from "@ngx-translate/core";

import { ApiService } from "../services/api.service";

import { LoginResult, UserStorage } from "../interfaces/user-storage";
import { GeneralVariable } from "../../general-variable";

@Injectable()
export class UserService {
  public login_error;
  private _userStorageKey: string = "current-user";
  private _tokenStorageKey: string = "token";
  private _requestStorageKey: string = "ticket-request-type";

  constructor(
    private _http: HttpClient,
    private _router: Router,
    private _storage: LocalStorageService,
    private _notify: NotificationsService,
    private _translate: TranslateService,
    public generalVariable: GeneralVariable,
    private _api: ApiService
  ) {}

  public userStorage(getToken?: boolean): any {
    if (getToken) return this._storage.get(this._tokenStorageKey) || "";
    return (
      this._storage.get<UserStorage>(this._userStorageKey) ||
      ({
        user: {
          fullname: this._translate.currentLang == "fa" ? "میهمان" : "Guest",
          email: "",
          is_admin: false,
        },
      } as UserStorage)
    );
  }
  public language = "";
  public getLang() {
    this._translate.onLangChange.subscribe((data) => {
      this.language = data.lang;
    });
    return this.language;
  }

  public getUserName(): string {
    return this.userStorage().user.name || this.userStorage().user.username;
  }

  public setUserStorage(userStorage: LoginResult): void {
    if (userStorage.token)
      this._storage.set(this._tokenStorageKey, userStorage.token);
    this._storage.set(this._userStorageKey, userStorage);
  }

  public updateUserStorage(): void {
    this._api.set(
      "user/current",
      "GET",
      { notify: false },
      (res: LoginResult): void => {
        this.setUserStorage(res);
      },
      (err) => {
        this._notify.remove();
      }
    );
  }

  public isLoggedIn(redirectUrl?: string): boolean {
    if (this._storage.get(this._userStorageKey)) return true;
    if (redirectUrl)
      this._api.set(
        "user/current",
        "GET",
        { notify: false },
        (res: LoginResult): void => {
          this.passToPanel(res, redirectUrl);
        }
      );
    return false;
  }

  public isAdmin(): boolean {
    if(this.userStorage().user.role != 'member') {
      return true;
    }
  }

  public login(body: { username?: string; password?: string }) {
    this._api.set(
      "user/login",
      "POST",
      {
        body: body,
      },
      (res: LoginResult): void => {
        // debugger
        // TODO: Redirect after login when user comes through external link
        if(res.user['role'] == 'member') {
          this.passToPanel(res, '/books');
        } else {
          this.passToPanel(res, '/dashboard');
        }
        this.setUserStorage(res);
        // this.updateUserStorage();
        // this.getRequestTypes();
        this._translate.get("notify.login-success").subscribe((text) => {
          this._notify.success(text.title, text.message);
        });
      },
      (err) => {}
    );
  }

  public register(body: {
    fullname: string;
    email: string;
    password: string;
  }): void {
    this._api.set(
      "auth/register",
      "POST",
      {
        body: body,
      },
      (res: LoginResult): void => {
        this.passToPanel(res);
        this.updateUserStorage();
        this._translate.get("notify.register-success").subscribe((text) => {
          this._notify.success(text.title, text.message);
        });
      }
    );
  }

  public passToPanel(loginResult: LoginResult, redirectUrl?: string): void {
    this.setUserStorage(loginResult);
    if (redirectUrl) this._router.navigateByUrl(redirectUrl);
    else this._router.navigate(["/"]);
  }

  public logout(): void {
    this._api.set(
      "user/logout",
      "GET",
      {},
      (res: { redirect_url: string }) => {
        // this.ejectUser({ redirect_url: res.redirect_url });
        this.ejectUser({ redirect_url: "/auth", notify: true });
      }
    );
  }

  public ejectUser(opt: { redirect_url?: string; notify?: boolean }): void {
    this._notify.remove();
    this._storage.remove(this._userStorageKey);
    this._storage.remove(this._tokenStorageKey);
    this._storage.remove(this._requestStorageKey);

    if (opt.notify)
      this._translate.get("notify.login-required").subscribe((text) => {
        this._notify.warn(text.title, text.message);
      });

    if (opt.redirect_url && window.location.hostname !== "localhost") {
      window.location.replace(opt.redirect_url);
    } else {
      this._router.navigate(["/auth"]);
    }
  }

  public userAccessible(page, action: string): boolean {
    let access = false;
    let feature = [];
    let userAccess: any;
    if (this.isAdmin()) {
      return true;
    } else {
      if (this.userStorage().user.roles != undefined) {
        this.userStorage().user.roles.forEach((role) => {
          userAccess = role.groups;
          if (userAccess.length) {
            [].forEach.call(userAccess, (g: ApiModel.Groups) => {
              g.features.forEach((f: ApiModel.Features) => {
                feature.push(f);
              });
            });
          }
        });
        let binaryArray = [];
        feature.forEach((f: ApiModel.Features) => {
          if (page == f.name) {
            if (f.actions !== null) {
              if (f.actions.includes(action)) {
                binaryArray.push(1);
              } else {
                binaryArray.push(0);
              }
            }
          }
        });
        return binaryArray.includes(1);
      }
    }
  }
  public checkAccess(page, action: string, existItem?: string[]): boolean {
    if (existItem) {
      let exist = existItem.filter((p) => {
        if (this.userAccessible(p, "read")) return p;
      });
      return !!exist.length;
    }
    return this.userAccessible(page, action);
  }
  public getRequestTypes() {
    this._api.set(
      "coding/ticket_request_type",
      "GET",
      { id: "get:requestType" },
      (res) => {
        this._storage.set(this._requestStorageKey, res.coding.children);
      }
    );
  }

  public accessIssueTypeNames = [];
  public requestAccessible(page: string): boolean {
    if (this.isAdmin()) {
      return true;
    // } else {
    //   if (this._storage.get(this._requestStorageKey)) {
    //     this.accessIssueTypeNames = this._storage.get(this._requestStorageKey);
    //     return this.compareFn(page);
    //   } else {
    //     return false;
    //   }
    }
  }

  public access_issue_type_ids = [];
  public pages = ["request", "list_request", "create_request", "view_request"];
  public compareFn(page: string) {
    let result = this.userStorage()
      .user.roles.filter((role) => role.access_issue_type_id)
      .map((role) => role.access_issue_type_id);
    this.access_issue_type_ids = Array.from(
      new Set([].concat.apply([], result))
    );
    this.accessIssueTypeNames = this.accessIssueTypeNames.filter((request) => {
      return this.access_issue_type_ids.indexOf(request.value1) > -1;
    });
    if (
      this.pages.includes(page) &&
      (this.accessIssueTypeNames.length ||
        this.userAccessible("requesting", "read"))
    ) {
      return true;
    }
    let binaryArray = [];
    this.accessIssueTypeNames.map((issue) => {
      if (issue.value2 == page) {
        binaryArray.push(1);
      } else {
        binaryArray.push(0);
      }
    });
    return binaryArray.includes(1);
  }
}
