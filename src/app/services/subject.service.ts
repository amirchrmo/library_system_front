import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SubjectService {

  public monitoringObject = new Object();
  public monitoringDataFromEditNode = new Object();

  public exportData: Object[] = [];
  public reportName: string;
  public reportDate: string;
  public reportId: string;
  public field_sort: string[] = [];
  public selectedFilters: any[] = [];

constructor() {
  
 }

 setMonitoringObject(monitoringObject) {
   this.monitoringObject = monitoringObject;
 }

}
