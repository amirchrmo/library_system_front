import { Injectable } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { LocalStorageService } from "angular-2-local-storage";
import moment from "moment-jalali";
// import fa from "moment/src/locale/fa";
import { Location } from "@angular/common";

export interface Params {
  [key: string]: any;
}

declare var persianDate: any;

@Injectable({
  providedIn: "root",
})
export class QueryService {
  private _ignores: Array<string> = ["limit"];

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _storage: LocalStorageService,
    private _location: Location
  ) {}

  public params(): Params {
    return this._route.snapshot.queryParams;
  }

  public param(key: string): any {
    return this.params()[key];
  }

  public set(params: Params, clear: boolean = false): Params {
    if (!clear) params = Object.assign({}, this.params(), params);

    for (const key in params) {
      if (this._ignores.includes(key) || (!params[key] && params[key] !== 0))
        delete params[key];
    }

    this._router.navigate([], { queryParams: params });
    return params;
  }

  public remove(key: string): Params {
    let params = this.params();
    delete params[key];
    return this.set(params, true);
  }

  public clear(): void {
    this.set({}, true);
  }
  public backToPage() {
    this._location.back();
  }

  public convertDate(date: string, time?: boolean): string {
    let lang = this._storage.get("lang");
    let ts_date = this.convertUnix(date);
    if (ts_date > 0) {
      switch (lang) {
        case "fa": {
          if (date != "") {
            time
              ? (date = moment(date).format("HH:mm:ss - jYYYY/jMM/jDD"))
              : (date = moment(date).format("jYYYY/jMM/jDD"));
          } else {
            time
              ? (date = moment().format("HH:mm:ss - jYYYY/jMM/jDD"))
              : (date = moment().format("jYYYY/jMM/jDD"));
          }
          break;
        }
        case "en": {
          if (date != "") {
            time
              ? (date = moment(date).format("YYYY/MM/DD - HH:mm:ss"))
              : (date = moment(date).format("YYYY/MM/DD"));
          } else {
            time
              ? (date = moment().format("YYYY/MM/DD - HH:mm:ss"))
              : (date = moment().format("YYYY/MM/DD"));
          }

          break;
        }
        default: {
          break;
        }
      }
    } else {
      date = "-";
    }
    return date;
  }
  public convertDateForPrint(date: string, print?: boolean): string {
    let lang = this._storage.get("lang");
    let ts_date = this.convertUnix(date);
    if (ts_date > 0) {
      switch (lang) {
        case "fa": {
          if (date != "") {
            print
              ? (date = moment(date).format("HH:mm:ss - jYYYY/jMM/jDD"))
              : (date = moment(date).format("jYYYY/jMM/jDD"));
          } else {
            print
              ? (date = moment().format("HH:mm:ss - jYYYY/jMM/jDD"))
              : (date = moment().format("jYYYY/jMM/jDD"));
          }
          break;
        }
        case "en": {
          if (date != "") {
            print
              ? (date = moment(date).format("YYYY/MM/DD - HH:mm:ss"))
              : (date = moment(date).format("YYYY/MM/DD"));
          } else {
            print
              ? (date = moment().format("YYYY/MM/DD - HH:mm:ss"))
              : (date = moment().format("YYYY/MM/DD"));
          }

          break;
        }
        default: {
          break;
        }
      }
    } else {
      date = "-";
    }
    return date;
  }
  public toStringDate(date) {
    return date.toString().slice(0, -3);
  }
  public convertUnix(date): number {
    return moment(date).format("x");
  }
  public convertUnixToDate(unix, time?: boolean): string {
    let lang = this._storage.get("lang");
    let date = "-";
    if (unix > 0) {
      switch (lang) {
        case "fa":
          date = time
            ? moment.unix(unix).format("HH:mm:ss - jYYYY/jMM/jDD")
            : moment.unix(unix).format("jYYYY/jMM/jDD");
          break;
        case "en":
          date = time
            ? moment.unix(unix).format("YYYY/MM/DD - HH:mm:ss")
            : moment.unix(unix).format("YYYY/MM/DD");
          break;
      }
    }
    return date;
  }

  public oneMonthAgo(date): string {
    let newDate = moment(date).subtract(1, "month");
    return moment(newDate).subtract(1, "day").format("YYYY/MM/DD");
  }

  public converToTimePass(date): string {
    let lang = this._storage.get("lang");

    let time = moment(),
      endTime = moment(time),
      startTime = moment(date),
      dif = endTime.diff(startTime),
      du = moment.duration(dif),
      years = du.years(),
      days = du.days(),
      months = du.months(),
      hours = du.hours(),
      minutes = du.minutes(),
      seconds = du.seconds(),
      prevHours = "",
      yearsAgo = "",
      monthsAgo = "",
      daysAgo = "";
    switch (lang) {
      case "fa":
        yearsAgo = years ? `${years} سال ` : "";
        monthsAgo = months ? `${months} ماه و` : "";
        daysAgo = days ? `${days} روز و ` : "";
        prevHours = yearsAgo + monthsAgo + daysAgo;
        break;
      case "en":
        yearsAgo = years ? `${years} year(s)` : "";
        monthsAgo = months ? `${months} month(s)` : "";
        daysAgo = days ? `${days} day(s)` : "";
        prevHours = yearsAgo + monthsAgo + daysAgo;
        break;
      default:
        break;
    }
    return `${prevHours} ${hours}:${minutes}:${seconds}`;
  }
  public deleteEmptyFieldsFromObjec(objec: {}): Object {
    for (const probName in objec) {
      if (
        objec[probName] == null ||
        objec[probName] == "" ||
        objec[probName] == [] ||
        objec[probName] == {} ||
        objec[probName] == undefined ||
        objec[probName].length == 0
      ) {
        delete objec[probName];
      }
    }
    return objec;
  }
  public scroll(el) {
    if (el.length) {
      el[0].scrollIntoView({
        behavior: "smooth",
        inline: "nearest",
      });
    }
  }
  public scrollToEll(elementName: string, type: string): void {
    setTimeout(() => {
      let nodeDetaileEl =
        type === "id"
          ? document.getElementById(elementName)
          : document.getElementsByClassName(elementName);
      this.scroll(nodeDetaileEl);
    }, 1000);
  }
  public setRangeIp(ip_pools) {
    let lanRangeIP = ip_pools
      .filter((item) => item.lan === true)
      .map((ip_pool) => `${ip_pool.from} - ${ip_pool.to}`)
      .join(" , ");
    let wanRangeIP = ip_pools
      .filter((item) => item.lan === false)
      .map((ip_pool) => `${ip_pool.from} - ${ip_pool.to}`)
      .join(" , ");
    return { wanRangeIP, lanRangeIP };
  }
}
