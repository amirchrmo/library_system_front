import { Injectable } from "@angular/core";
import { forkJoin } from "rxjs";

import {
  NotificationsService,
  NotificationType as Notify,
} from "angular2-notifications";
import { TranslateService } from "@ngx-translate/core";
import swal from "sweetalert2";

export { Notify };

export enum Opr {
  Create = "create",
  Enable = "enable",
  Disable = "disable",
  Add = "add",
  Edit = "edit",
  Update = "update",
  Update_fail = "update-fail",
  Select = "select",
  Del = "delete",
  BatchDel = "batch-delete",
  Remove = "remove",
  RemoveFromList = "remove-from-list",
  Swap = "swap",
  Rcal = "recalculate",
  Upload = "upload",
  Save = "save",
  Send = "sent",
  Reboot = "reboot",
  Fail = "reboot-fail",
  Assign = "assign",
  Discover = "discover",
  Discover_fail = "discover-fail",
  InterfaceDiscover = "interface_discover",
  Exec = "execute",
}

export enum Ent {
  File = "file",
  UserProfile = "user-profile",
  Group = "group",
  Role = "role",
  User = "user",
  Node = "node",
  Incident = "incident",
  Request = "request",
  GroupRequest = "group-request",
  Apn = "apn",
  Sla = "sla",
  Customer = "customer",
  Info = "information",
  Cpe = "cpe",
  Reason = "reason",
  Kpi = "kpi",
  Interface = "interface",
  Acs = "acs",
  Comment = "comment",
  Action = "action",
  BulkNode = "bulk-node",
  Field = "Field",
  Filter = "Filter",
  DynamicReport = "dynamicReport",
  notification = "notification",
  params = "params",
  monitoring = "monitoring",
  Sensor = "sensor",
  Again = "again",
  setting = "setting",
  logs = "logs",
}

export enum SwlT {
  Success = "success",
  Warn = "warning",
  Error = "error",
  Info = "info",
  Question = "question",
}

@Injectable()
export class NotifyService {
  constructor(
    private _notify: NotificationsService,
    private _translate: TranslateService
  ) {}

  public status(
    operation: Opr,
    entity: Ent,
    name?: string,
    type: Notify = Notify.Success,
    textKey: string = "success"
  ): void {
    let operation$ = this._translate.get(`operation.${operation}`);
    let entity$ = this._translate.get(`entity.${entity}`);
    forkJoin([operation$, entity$]).subscribe((data1) => {
      const [operation, entity]: string[] = data1;
      let statusTitle$ = this._translate.get(`notify.${textKey}.title`, {
        operation: operation,
        entity: entity,
      });
      let statusMessage$ = this._translate.get(`notify.${textKey}.message`, {
        operation: operation,
        name: name || entity,
      });
      forkJoin([statusTitle$, statusMessage$]).subscribe((data2) => {
        let [title, message]: string[] = data2;
        this._notify.create(title, message, type);
      });
    });
  }

  public create(
    title: string,
    message: string,
    type: Notify = Notify.Success
  ): void {
    this._notify.create(title, message, type);
  }

  public confirm(
    operation: Opr,
    entity: Ent,
    cb: () => void,
    cancel?: () => void
  ): void {
    const operation$ = this._translate.get(`operation.${operation}`);
    let entity$ = this._translate.get(`entity.${entity}`);
    let message$ = this._translate.get(`confirm.message`);
    forkJoin([operation$, entity$, message$]).subscribe((data) => {
      let [operation, entity, message] = data;
      this.simpleConfirm(`${operation.title} ${entity}`, message, cb, cancel);
    });
  }

  // TODO: In this case (when using sweetalert), preferred to return promise instead of using callbacks!
  public simpleConfirm(
    title: string,
    message: string,
    cb: () => void,
    cancel?: () => void
  ): void {
    let options: { [key: string]: any } = {
      title: title,
      text: message,
      type: "warning",
      confirmButtonText: this._translate.currentLang == "fa" ? "بله" : "Yes",
      confirmButtonClass: "btn",
      showCancelButton: true,
      cancelButtonText: this._translate.currentLang == "fa" ? "خیر" : "No",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false,
      heightAuto: false,
      // reverseButtons: true
    };

    swal(options).then((result) => {
      if (result.value) cb();
      else {
        cancel();
      }
    });
  }

  public alert(
    title: string,
    message: string,
    type: SwlT = SwlT.Success
  ): Promise<any> {
    return swal({
      title: title,
      text: message,
      type: type,
      confirmButtonText: this._translate.currentLang == "fa" ? "تأیید" : "OK",
      confirmButtonClass: "btn",
      heightAuto: false,
      buttonsStyling: false,
    });
  }
}
