{
  "$schema": "http://json-schema.org/draft-07/schema",
  "$id": "filters_schema",
  "title": "Filters",
  "description": "Filter params that send to the server when requesting data",
  "type": "array",
  "definitions": {
    "filter": {
      "required": ["type", "key", "label"],
      "properties": {
        "type": {
          "type": "string",
          "title": "Filter type"
        },
        "key": {
          "type": "string",
          "title": "Value key",
          "description": "The key that will store values on that"
        },
        "label": {
          "type": "string",
          "title": "Filter label",
          "description": "The text that will be show on top of filter"
        }
      }
    },
    "select": {
      "allOf": [
        {
          "$ref": "#/definitions/filter"
        },
        {
          "properties": {
            "type": {
              "enum": ["select"]
            },
            "data": {
              "type": "array",
              "title": "Filter Data",
              "description": "Data items that user can select them",
              "minItems": 1,
              "items": {
                "title": "Data Item",
                "oneOf": [
                  { "type": "string" },
                  {
                    "type": "object",
                    "required": ["id", "text"],
                    "properties": {
                      "id": {
                        "oneOf": [
                          { "type": "number" },
                          { "type": "string" }
                        ]
                      },
                      "text": { "type": "string" },
                      "selected": {
                        "type": "boolean",
                        "default": false
                      },
                      "disabled": {
                        "type": "boolean",
                        "default": false
                      }
                    },
                    "additionalProperties": false
                  }
                ]
              }
            },
            "data_uri": {
              "type": "string",
              "title": "Data URI",
              "description": "Use to load data from static json file"
            },
            "ajax": {
              "type": "object",
              "title": "AJAX Configs",
              "description": "Use when need to load items from API dynamically",
              "required": ["url"],
              "properties": {
                "url": {
                  "type": "string",
                  "title": "API URI"
                },
                "delay": {
                  "type": "number",
                  "title": "Request delay",
                  "description": "Delay time in milliseconds after input change for request, to avoid multiple requests",
                  "examples": [1000]
                },
                "term_key": {
                  "type": "string",
                  "title": "Term Key",
                  "default": "term"
                },
                "term_pattern": {
                  "type": "string",
                  "title": "Term Pattern",
                  "examples": [".*{{term}}"]
                }
              },
              "additionalProperties": true
            },
            "schema": {
              "type": "object",
              "required": [],
              "properties": {
                "id": { "type": "string" },
                "text": { "type": "string" },
                "selected": { "type": "string" },
                "disabled": { "type": "string" },
                "res_key": {
                  "type": "string",
                  "title": "Results Data Key",
                  "examples": ["data"]
                }
              }
            },
            "placeholder": {
              "type": "string",
              "title": "Placeholder",
              "description": "The text that will be show on the search box of select filter"
            },
            "language": {
              "type": "string",
              "title": "Language"
            },
            "minimumInputLength": {
              "type": "number",
              "default": 0
            },
            "multiple": {
              "type": "boolean",
              "default": true,
              "title": "Multiple select items",
              "description": "Just for select type"
            },
            "allowClear": {
              "type": "boolean",
              "default": true,
              "title": "Allow clear items",
              "description": "Just for select type"
            },
            "closeOnSelect": {
              "type": "boolean",
              "default": false,
              "title": "Close items after select",
              "description": "Just for select type"
            }
          },
          "dependencies": {
            "data_uri": [],
            "ajax": ["schema"]
          }
        }
      ]
    },
    "range": {
      "allOf": [
        {
          "$ref": "#/definitions/filter"
        },
        {
          "properties": {
            "type": {
              "enum": ["range", "range-inputs"]
            },
            "range": {
              "type": "object",
              "title": "Rnges and Steps",
              "required": ["min", "max"],
              "properties": {
                "min": {
                  "type": "number",
                  "default": 0
                },
                "max": {
                  "type": "number",
                  "default": 100
                }
              },
              "additionalProperties": {
                "type": "number"
              }
            },
            "start": {
              "type": "array",
              "title": "Range handles position",
              "description": "Default value for each range handles at first,\nJust for range type",
              "minItems": 2,
              "maxItems": 2,
              "items": {
                "type": "integer"
              }
            },
            "step": {
              "type": "integer",
              "default": 1,
              "title": "Range steps",
              "description": "Just for range type"
            }
          }
        }
      ]
    }
  },
  "items": {
    "type": "object",
    "oneOf": [
      {
        "$ref": "#/definitions/select"
      },
      {
        "$ref": "#/definitions/range"
      }
    ]
  }
}
